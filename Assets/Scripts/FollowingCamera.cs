﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowingCamera : MonoBehaviour
{
    [SerializeField] GameObject target;
    Vector3 vector = new Vector3(8f, 2f, 0f);

    void FixedUpdate()
    {       
        gameObject.transform.position = target.transform.position + vector;
    }
}
