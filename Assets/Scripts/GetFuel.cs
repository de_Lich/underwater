﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetFuel : MonoBehaviour
{
    public float fuel = 100f;
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            FindObjectOfType<Rocket>().currentFuel += fuel;
            Destroy(gameObject);
            Debug.Log("!!!");
        }
    }
}
