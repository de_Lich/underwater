﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrelSpawned : MonoBehaviour
{
    float xDistance;
    float zDistance;
    public float yDistance;

    public float zLeft, zRight;

    public float timeSpawned = 1f;
    [SerializeField] GameObject []trash;

    void Start()
    {
        StartCoroutine(trashSpawn());
    }
    IEnumerator trashSpawn()
    {
        Vector3 randomPos = Vector3.zero;
        while (true)
        {
            xDistance = Random.Range(4.5f,5.5f);
            zDistance = Random.Range(zLeft, zRight);
            float xRot = Random.Range(0f, 180f);
            float yRot = Random.Range(0f, 180f);
            float zRot = Random.Range(0f, 180f);

            randomPos = new Vector3(xDistance, yDistance, zDistance);
            Instantiate(trash[Random.Range(0, trash.Length)], randomPos, Quaternion.Euler(xRot,yRot,zRot));
            yield return new WaitForSeconds(timeSpawned);
        }
    }
}
