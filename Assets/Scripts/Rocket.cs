﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocket : MonoBehaviour
{
    [SerializeField] float mainThrust = 1000f;
    [SerializeField] float rotate = 30f;
    [SerializeField] public float Maxfuel = 100f;
    [SerializeField] public float currentFuel;

    public FuelBar fuelBar;
    public bool isControlEnabled;
    Rigidbody rigidbody;
    AudioSource audiosource;
    [SerializeField] ParticleSystem engineFX;
    [SerializeField] ParticleSystem leftEngine;
    [SerializeField] ParticleSystem rightEngine;
    [SerializeField] AudioClip mainEngine;
    void Start()
    {
        audiosource = GetComponent<AudioSource>();
        rigidbody = GetComponent<Rigidbody>();
        isControlEnabled = true;
        currentFuel = Maxfuel;
        fuelBar.SetMaxFuel(Maxfuel);
    }

    void Update()
    {
        if (isControlEnabled)
        {
            Thrust();
            Rotate();
        }
        if (currentFuel <= 0.0f)
        {
            isControlEnabled = false;
            audiosource.Stop();
            engineFX.Stop();
        }
        if (currentFuel > Maxfuel)
        {
            currentFuel = Maxfuel;
        }
    }

    private void Rotate()
    {
        float speed = rotate * Time.deltaTime;
        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(Vector3.left * speed);
            if (!leftEngine.isEmitting) { leftEngine.Play(); }
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(Vector3.right * speed);
            if (!rightEngine.isEmitting) { rightEngine.Play(); }
        }
        else
        {
            rightEngine.Stop();
            leftEngine.Stop();
        }
    }

    private void Thrust()
    {
        float fuelIntake = 2f;
        if (Input.GetKey(KeyCode.Space))
        {
            if (!audiosource.isPlaying)
            {
                audiosource.PlayOneShot(mainEngine); 
            }
            if (!engineFX.isEmitting)
            {
                engineFX.Play();
            }
            currentFuel -= fuelIntake * Time.deltaTime;
            if(currentFuel <= 8f && currentFuel >= 7f)
            {
                FindObjectOfType<AudioManager>().Play("lowFuel");
            }
            fuelBar.SetFuel(currentFuel);
            rigidbody.AddRelativeForce(Vector3.up * mainThrust * Time.deltaTime);
        }
        else
        {
            engineFX.Stop();
            audiosource.Stop();
        }
    }
}
