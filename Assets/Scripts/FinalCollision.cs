﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalCollision : MonoBehaviour
{
    [SerializeField] ParticleSystem winFX;
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            winFX.Play();
            
            Debug.Log("!!!");
        }
    }
}
