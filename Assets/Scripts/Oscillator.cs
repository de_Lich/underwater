﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[DisallowMultipleComponent]
public class Oscillator : MonoBehaviour
{
    [SerializeField] Vector3 movVector = new Vector3(0f, 0f, 0f);
    [SerializeField] [Range(0, 1)] float movFactor;
    [SerializeField] float period = 2f;

    Vector3 startingPos = new Vector3(0, 0, 0);
    void Start()
    {
        startingPos = transform.position;
    }

    void Update()
    {
        if (period <= Mathf.Epsilon) { return; }
        float cycles = Time.time / period;

        const float tau = Mathf.PI * 2;
        float rawSinWave = Mathf.Sin(cycles * tau);

        movFactor = rawSinWave / 2f;
        Vector3 offSet = movVector * movFactor;
        transform.position = startingPos + offSet;
    }
}
