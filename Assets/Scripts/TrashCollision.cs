﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrashCollision : MonoBehaviour
{
    private float timer = 3f;
    private float destroyTimer = 30f;
    public enum TrashType
    {
        Bottle,
        Box,
        Barrel
    }
    public TrashType trash;
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            switch (trash)
            {
                case TrashType.Box:
                    CollisionWithBox(collision);
                    break;
                case TrashType.Bottle:
                    CollisionWithBottle();
                    break;
                case TrashType.Barrel:
                    break;
                default:
                    break;
            }           
        }
        else if(collision.gameObject.tag == "Ground")
        {
            Invoke("CollisionWithGround",timer);
        }
    }

    private void CollisionWithGround()
    {
        var rbDrag = GetComponent<Rigidbody>();
        rbDrag.drag = 30f;
        rbDrag.mass = 5.0f;
        GetComponent<Collider>().enabled = false;
        Destroy(gameObject,destroyTimer);
    }

    private static void CollisionWithBottle()
    {
        FindObjectOfType<AudioManager>().Play("bottle");
    }

    private static void CollisionWithBox(Collision collision)
    {
        collision.rigidbody.AddForce(0f, -4f, 0f, ForceMode.VelocityChange);
        FindObjectOfType<AudioManager>().Play("box");
    }
}
