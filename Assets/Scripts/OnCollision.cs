﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OnCollision : MonoBehaviour
{
    [SerializeField] Text levelNumber;
    [SerializeField] ParticleSystem blastFX;
    int currentSceneIndex;
    private void Start()
    {
        currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
    }
    private void Update()
    {
        int level = SceneManager.GetActiveScene().buildIndex;
        levelNumber.text = level.ToString();
    }
    private void OnCollisionEnter(Collision collision)
    {
        switch (collision.gameObject.tag)
        {
            case "Finish":
                SuccessCondition();
                break;
            case "Friendly":
                return;
            default:
                DeathCondition();
                break;
        }
    }

    private void DeathCondition()
    {
        FindObjectOfType<AudioManager>().Play("PlayerDeath");
        if (!blastFX.isPlaying) { blastFX.Play(); }
        FindObjectOfType<Rocket>().isControlEnabled = false;
        Invoke("Death", 2f);
    }

    private void SuccessCondition()
    {
        FindObjectOfType<AudioManager>().Play("winSFX");
        FindObjectOfType<Rocket>().isControlEnabled = false;
        Invoke("Success", 2f);
    }

    private void Success()
    {
        int nextScene = currentSceneIndex + 1;
        if (nextScene == SceneManager.sceneCountInBuildSettings) { nextScene = 0; }
        SceneManager.LoadScene(nextScene);
    }

    void Death()
    {
        SceneManager.LoadScene(currentSceneIndex);
    }
}
